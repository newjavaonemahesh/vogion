<?php
/**
 * Plugin Name: MV My Recent Category Posts
 * Description: Displays recent posts by category slug
 * Version: 1.0
 * Author:  Mahesh
 * Author URI: https://www.example.com
 * Text Domain: mv-my-category-posts
 */

if (!function_exists('add_action')) {
    echo 'Not allowed!';
    exit();
}

// Setup




// Includes
include('includes/activate.php');
include('includes/init.php');
include('includes/shortcodes/mv-my-category-posts.php');



// Hooks
register_activation_hook(__FILE__, 'mv_mcp_activate_plugin');
add_action('init', 'mv_mc_category_posts_init');



// Shortcodes
add_shortcode('mv_mc_category_posts', 'mv_my_category_posts_shortcode');