<?php

function mv_my_category_posts_shortcode($atts){

    global $post;

    extract(shortcode_atts(array(
        'catname' => '',
    ), $atts));

    $mv_mc_catp_args = array(
        'post_type'           =>  'post',
        //limit posts to 3
        'posts_per_page'    => 3,
        'ignore_sticky_posts' => true,
        'category_name'    => $catname,
        'orderby' => array(
            'comment_count' => 'DESC',
            'date'          => 'DESC',
        )
    );


    $myposts = get_posts($mv_mc_catp_args);

    if (!empty($myposts)) :

        $catObj = get_category_by_slug($catname);
        $catid = $catObj->term_id;
        $display_category = $catObj->name;

        ?>
        <ul class="featured noul">
        <li class="title">
            <a href="<?php get_category_link($catid);?>" title="<?php echo $display_category; ?>"><?php echo $display_category; ?>
            </a>
        </li>
        <?php

        foreach ( $myposts as $c_post ) :
            $post = $c_post;
            setup_postdata( $post );
    ?>




                <li class="entry">

                    <?php
                    $mv_my_category_posts_thumb = mv_my_category_posts_get_thumb($post->ID, "mv_mc_category_posts");
                    ?>

                    <a href="<?php the_permalink();?>" class="thumb"><img src="<?php echo $mv_my_category_posts_thumb;?>" class="attachment-post-thumbnail  wp-post-image nelioefi" alt=""></a>
                    <h1><a href="<?php the_permalink();?>"><?php echo get_the_title();?></a></h1>
                    <div class="meta">
                        <span class="date"><?php the_time('F j, Y'); ?></span>
                    </div>

                    <div class="clear"> </div>
                </li>


    <?php endforeach;
        wp_reset_postdata();

    else :
        echo 'Sorry, no posts found for category';
        endif;

    ?>
    </ul>
<?php
}

function mv_my_category_posts_get_thumb($post_id, $position){
    $thumb = get_the_post_thumbnail($post_id, $position);
    $thumb = explode("\"", $thumb);
    return $thumb[5];
}





