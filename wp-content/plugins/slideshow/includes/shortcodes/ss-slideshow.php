<?php

function ss_slideshow_shortcode(){

    global $post;

    $ss_sort = "post_date";
    $ss_order = "DESC";
    $ss_limit = 6;
    $ss_points = "...";
    $ss_post_limit = "-1";

    $args = array(
        'meta_key' => 'add_to_slideshow',
        'meta_value'=> 1,
        'suppress_filters' => 0,
        'post_type' => array('post', 'page'),
        'orderby' => $ss_sort, 'order' => $ss_order,
        'numberposts'=> $ss_limit);

    $myposts = get_posts( $args );

    if ( ! empty( $myposts ) ) {

        ?>

        <div class="slideshow">

        <?php

        foreach( $myposts as $post ) :	setup_postdata($post);

            ?>
            <div class="slide">
                <a href="<?php the_permalink();?>" class="thumb">

                    <?php
                        $ss_slider_thumb = ss_slider_get_thumb($post->ID, "ss_slideshow");
                    ?>

                    <img src="<?php echo $ss_slider_thumb?>">
                <span><?php echo get_the_title();?></span>
                </a>

                <div class="meta">
                    <span class="date"><?php the_time('F j, Y'); ?></span> —
                    <span class="category"><?php the_category(',');?></span>
                </div>


            </div>

            <?php
        endforeach; wp_reset_postdata(); ?>

        </div>


        <script type="text/javascript">
            //<![CDATA[
            var ide_autoslide = true;
            //]]>
        </script>

        <?php
    }

?>



<?php

}

function ss_slider_get_thumb($post_id, $position) {
    $thumb = get_the_post_thumbnail($post_id, $position);
    $thumb = explode("\"", $thumb);
    return $thumb[5];

}