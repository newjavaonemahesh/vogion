<?php

function ss_create_metaboxes(){
    add_meta_box(
        'ss_options_mb',
        __('Slideshow Options', 'slideshow'),
        'ss_options_mb',
        'post',
        'normal',
        'high'
    );

}