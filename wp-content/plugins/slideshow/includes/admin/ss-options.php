<?php

function ss_options_mb($post){

    $ss_options_data                        =   get_post_meta($post->ID, 'add_to_slideshow', true);

    if (!$ss_options_data) {
        $ss_options_data            = 0;
    }

    ?>

    <div class="inside">
        <table class="form-table">
            <tr>
                <th><label for="ss_addToSlideshow">Add to Slideshow?</label></th>
                <td><input type="checkbox" name="ss_addToSlideshow" value="1" <?php if($ss_options_data == 1) { echo "checked='checked'";} ?>></td>
            </tr>
        </table>
    </div>

<?php
}