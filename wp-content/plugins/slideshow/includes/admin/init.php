<?php

function ss_admin_init(){

    include('create-metaboxes.php');
    include('ss-options.php');

    add_action('add_meta_boxes_post', 'ss_create_metaboxes');
}