jQuery(function($){
    $.isset = function(name) {
        return (typeof(window[name]) !== 'undefined') ? true : false;
    };

    var ecoSlide = {
        dots: $('<ul class="noul dots">'),
        active: false,
        current: 0,
        current_slide: null,

        init: function (container, slide_name) {

            $(container).append($(this.dots));

            $(container + ' ' + slide_name).each(function () {
                var slide = $(this);

                var a = $('<a href="#">&#9679;</a>');
                $(a).click(function () {
                    if (ecoSlide.active) return false;

                    var index = $('.dots li').index($(this).parent());
                    if(ecoSlide.current == index) return false;

                    $(ecoSlide.dots).find('a').removeClass('sel');
                    $(this).addClass('sel');

                    $(ecoSlide.current_slide).animate({
                        'opacity': 0.0
                    }, 500, function() {
                        $(this).hide();
                    });

                    $(slide).show();
                    $(slide).animate({opacity: 1.0}, 500, function() {
                        ecoSlide.active = false;
                    });


                    ecoSlide.current_slide = slide;
                    ecoSlide.active = true;

                    ecoSlide.current = index; //$(container + ' ' + slide_name).index(ecoSlide.current_slide);
                    return false;
                });

                // add dots
                $(a).each(function () {
                    $(ecoSlide.dots).append($('<li>').append(this));
                });
            });

            $(container + ' ' + slide_name + ':gt(0)').css('opacity', 0.0);

            ecoSlide.current_slide = $(container + ' ' + slide_name + ':first').show();
            $(this.dots).find('a:first').addClass('sel');

        }
    };

    // initialize the slides
    ecoSlide.init('.slideshow', '.slide');
    if($.isset('ide_autoslide') && ide_autoslide) {
        var dots = $('.dots a');

        var cur = 0;
        window.setInterval(function() {
            if(ecoSlide.current >= $(dots).length - 1) {
                cur = 0;
            } else {
                cur = ecoSlide.current+1;
            }
            $(dots[cur]).click();
        }, 15000);
    }


    // try to auto-resize large blog post images so that they don't overflow
    if($.isset('ide_img_resize') && ide_img_resize) {
        $('.widget_links img').each(function() {
            $(this).parent().parent().parent().addClass('widgetbanners');
        });

        $('.post .text').each(function() {
            var maxWidth = $(this).outerWidth();
            var maxHeight = $(this).outerHeight();

            $(this).find('img').each(function() {

                var width = $(this).width();
                var height = $(this).height();

                var ratio = height / width;
                if(width >= maxWidth){
                    width = maxWidth;
                    height = width * ratio;
                } else if(height >= maxHeight){
                    height = maxHeight;
                    width = height / ratio;
                }

                $(this).css('width', width);
                $(this).css('height', height);
            });
        });
    }
});