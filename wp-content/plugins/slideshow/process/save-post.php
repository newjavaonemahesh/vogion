<?php

function ss_save_post_admin($post_id, $post, $update){
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    if (!$update) {
        return;
    }

    $ss_options_data                                = 0;
    $ss_options_data            =   absint ($_POST['ss_addToSlideshow']);

    update_post_meta($post_id, "add_to_slideshow", $ss_options_data);
}