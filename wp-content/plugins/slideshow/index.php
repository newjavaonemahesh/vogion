<?php
/**
 * Plugin Name: Slideshow
 * Description: Extends wordpress, creates a slideshow from select posts
 * Version: 1.0
 * Author:  Mahesh
 * Author URI: https://www.example.com
 * Text Domain: slideshow
 */

if (!function_exists('add_action')) {
    echo 'Not allowed!';
    exit();
}

// Setup
define('SLIDESHOW_PLUGIN_URL', __FILE__);



// Includes
include('includes/activate.php');
include('includes/init.php');
include('includes/admin/init.php');
include('process/save-post.php');
include('includes/front/enqueue.php');
include('includes/shortcodes/ss-slideshow.php');

// Hooks
register_activation_hook(__FILE__, 'ss_activate_plugin');

add_action('init', 'ss_slideshow_init');

add_action('admin_init', 'ss_admin_init');
add_action('save_post', 'ss_save_post_admin', 10, 3);
add_action('wp_enqueue_scripts', 'ss_enqueue_scripts', 9999);



// Shortcodes
add_shortcode('ss_slideshow', 'ss_slideshow_shortcode');