<?php

function mv_mr_recent_posts_init(){

    $mv_mr_recp_img_width = 70;
    $mv_mr_recp_img_height = 90;
    add_image_size( 'mv_mr_recent_posts', $mv_mr_recp_img_width, $mv_mr_recp_img_height, true );

}