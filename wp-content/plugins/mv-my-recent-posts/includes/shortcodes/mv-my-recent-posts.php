<?php

function mv_my_recent_posts_shortcode($atts){
    global $post;


    extract(shortcode_atts(array(
        'posts' => 3,
    ), $atts));

    $mv_my_recp_args = array(
        'post_type'           =>  'post',
         //limit posts to 3
        'posts_per_page'    => $posts,
        'ignore_sticky_posts' => true,
        'orderby' => array(
            'comment_count' => 'DESC',
            'date'          => 'DESC',
        )
    );

    //$popular = new WP_Query('orderby=comment_count&posts_per_page='.$posts);
    $popular = new WP_Query($mv_my_recp_args);
    $mv_my_recent_posts = $popular->posts;

    if (!empty($mv_my_recent_posts)) :
        $recent_posts = $mv_my_recent_posts;

        ?>
        <ul class="strip noul">

        <?php
        if ( !empty($recent_posts) ) :
            foreach ( $recent_posts as $r_post ) :
                $post = $r_post;
                setup_postdata( $post );
            ?>

                <li>
                    <span class="date"><?php the_time('F j, Y'); ?></span>
                    <div class="clear"> </div>
                    <a href="<?php the_permalink();?>" class="thumb">

                        <?php
                            $mv_my_recent_posts_thumb = mv_my_recent_posts_get_thumb($post->ID, "mv_mr_recent_posts");
                        ?>

                        <img src="<?php echo $mv_my_recent_posts_thumb?>">
                    </a>
                    <h4><a href="<?php the_permalink();?>"><?php echo get_the_title();?></a></h4>
                    <div class="clear"> </div>
                </li>

            <?php endforeach;
            wp_reset_postdata();
        else :
            echo 'Sorry, no recent posts were found';
        endif;
            ?>

        </ul>
        <div class="clear"> </div>

    <?php endif; ?>

<?php
}

function mv_my_recent_posts_get_thumb($post_id, $position) {

    $thumb = get_the_post_thumbnail($post_id, $position);
    $thumb = explode("\"", $thumb);
    return $thumb[5];

}