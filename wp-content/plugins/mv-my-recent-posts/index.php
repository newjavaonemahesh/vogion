<?php
/**
 * Plugin Name: MV My Recent Posts
 * Description: Displays recent and popular posts
 * Version: 1.0
 * Author:  Mahesh
 * Author URI: https://www.example.com
 * Text Domain: mv-my-recent-posts
 */

if (!function_exists('add_action')) {
    echo 'Not allowed!';
    exit();
}

// Setup




// Includes
include('includes/activate.php');
include('includes/init.php');
include('includes/shortcodes/mv-my-recent-posts.php');



// Hooks
register_activation_hook(__FILE__, 'mv_mrp_activate_plugin');
add_action('init', 'mv_mr_recent_posts_init');


// Shortcodes
add_shortcode('mv_mr_recent_posts', 'mv_my_recent_posts_shortcode');

