<?php get_header();?>

<div id="main">

    <div id="content">

        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>

                <div class="post">


                    <h1 class="title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>

                    <div class="meta">

                        <p class="author">
                            <?php the_category(',');?> |
                            <?php the_time('F j, Y'); ?> </p>

                        <div class="clear"></div>
                    </div>
                    <!-- .meta //-->

                    <div class="text">
                        <p><?php the_content();?></p>
                        <div class="clear"></div>
                    </div>
                    <!-- .text //-->

                </div><!-- .post //-->
            <?php
            }
        }
        ?>
        <div class="pagination">

        </div>


        <?php
        global $wp_query;

        $big = 999999999; // need an unlikely integer

        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
            'prev_next' =>  false,
            'type'  =>  'list'
        ) );
        ?>

    </div><!-- content //-->

    <ul id="sidebar" class="noul">
        <?php get_sidebar();?>
    </ul><!-- sidebar //-->
    <div class="clear"> </div>

</div><!-- main //-->
<?php get_footer();?>
