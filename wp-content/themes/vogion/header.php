<?php
    $theme_opts                     =       get_option('mv_opts');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php wp_title();?></title>

    <?php wp_head();?>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

</head>

<body <?php body_class(); ?>>

<div id="container">
    <div id="wrap">
        <div id="header">
            <div class="logo">

                <?php
                    if (!empty($theme_opts['logo_img'])) {
                        ?>
                        <a href="<?php echo home_url(); ?>"><img src="<?php echo $theme_opts['logo_img']?>" /></a>
                    <?php
                    } else {
                        ?>
                        <a href="<?php echo home_url(); ?>"><img src="<?php echo get_theme_file_uri('images/my_Logo1.png')?>" /></a>
                    <?php
                    }
                ?>


            </div><!-- logo //-->

            <?php get_search_form(); ?>

            <div class="nav">
                <?php
                wp_nav_menu(array(
                    'theme_location'            =>  'primary',
                    'container'                 =>  false
                ));
                ?>
            </div>

            <div class="clear"> </div>

        </div><!-- header //-->


        <?php
        wp_nav_menu(array(
            'theme_location'            =>  'categories',
            'container'                 =>  false,
            'menu_class'                =>  'nav_categories noul',
            'walker' => new Description_Walker()
        ));
        ?>

        <div class="clear"> </div>

        <ul>
            <div align="center">
            </div>
        </ul>


        <div class="top_banner">
            <div class="underdogmedia-inpage" style=" width: 728px; height: 90px; box-sizing: content-box;" ></div></div>
