<?php get_header();?>

<div id="main">

    <div id="content" class="page homepage">
        <div id="homepage">

                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>

                        <?php the_content();?>
                        <div class="clear"></div>
                    <?php
                    }
                }
                ?>

            </div><!-- page homepage //-->
        </div><!-- content //-->

        <ul id="sidebar" class="noul">
            <?php get_sidebar();?>
        </ul><!-- sidebar //-->
        <div class="clear"> </div>

    </div><!-- main //-->
    <?php get_footer();?>
