<?php

function mv_widgets(){
    register_sidebar(array(
        'name'                      =>          __('Vogion Theme Sidebar', 'vogion'),
        'id'                        =>          'mv_sidebar',
        'description'               =>          __('Sidebar for the theme Vogion', 'vogion'),
        'class'                     =>         '',
        'before_widget'             => '<li id="%1$s" class="widget %2$s">',
        'after_widget'              => '</li>',
        'before_title'              => '<h2 class="widgettitle">',
        'after_title'               => '</h2>'
    ));

    register_sidebar( array(
        'name'                      => 'Footer Sidebar 1',
        'id'                        => 'mv_footer-sidebar-1',
        'description'               => __('Footer Sidebar for the theme Vogion', 'vogion'),
        'class'                     =>         '',
        'before_widget'             => '<li id="%1$s" class="widget %2$s">',
        'after_widget'              => '</li>',
        'before_title'              => '<h2 class="widgettitle">',
        'after_title'               => '</h2>'
    ) );
}