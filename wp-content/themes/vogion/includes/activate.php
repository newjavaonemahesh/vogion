<?php

function mv_activate(){
    if (version_compare(get_bloginfo('version'), '4.2', '<')) {
        wp_die(__('You must have a minimum version of 4.2 to use this theme.'));
    }

    $theme_opts                =        get_option('mv_opts');

    if (!$theme_opts) {
        $opts            =        array(
            'logo_img'           =>   ''
        );

        add_option('mv_opts', $opts);
    }

}