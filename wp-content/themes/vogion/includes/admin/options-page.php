<?php

function mv_theme_opts_page(){

    $theme_opts               =     get_option('mv_opts');

    ?>

    <div id="theme-options-wrap">
        <div class="icon32" id="icon-tools"></div>
        <h2>My Theme Options</h2>
        <p>Take control of your theme, by overriding the default settings with your own specific preferences.</p>

        <?php
        if(isset($_GET['status']) && $_GET['status'] == 1) {
            ?>
            <div id="message" class="updated notice notice-success is-dismissible"><p>Success!</p></div>
        <?php
        }
        ?>

        <form method="post" action="admin-post.php">
            <input type="hidden" name="action" value="mv_save_options">
            <?php wp_nonce_field('mv_options_verify'); ?>
            <ul>
                <li>
                    <label><?php _e('Logo Image', 'vogion'); ?><span> *</span>:</label>
                    <input maxlength="100" size="100" type="text" name="mv_inputLogoImg" value="<?php echo $theme_opts['logo_img']; ?>"/>
                    <button type="button" id="mv_uploadLogoImgBtn"><?php _e('Upload', 'vogion'); ?></button>
                </li>

                <li><label><?php _e('Footer Text (HTML Allowed)', 'vogion'); ?><span> *</span>: </label>
                    <textarea rows="4" cols="100" maxlength="1500" size="250" name="mv_inputFooter"><?php echo stripslashes_deep($theme_opts['footer']); ?></textarea>
                </li>
            </ul>
            <p class="submit">
                <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
            </p>
        </form>
    </div>

    <?php
}