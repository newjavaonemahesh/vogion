<?php

function mv_admin_menus(){
    add_menu_page(
        'Vogion Theme Options',
        'Theme Options',
        'edit_theme_options',
        'mv_theme_opts',
        'mv_theme_opts_page'
    );
}