<?php

function mv_admin_enqueue(){

    if (!isset($_GET['page']) || $_GET['page'] != 'mv_theme_opts') {
        return;
    }

    wp_register_script('mv_options', get_template_directory_uri() . '/options.js', array(), false, true );
    wp_enqueue_media();
    wp_enqueue_script('mv_options');

}