<div id="footer">
    <div class="content">
        <ul id="footerbar" class="noul">
                    <?php
                    if (is_active_sidebar('mv_footer-sidebar-1')){
                        dynamic_sidebar('mv_footer-sidebar-1');
                    }
                    ?>
        </ul><!-- footerbar //-->
        <div class="clear"> </div>
    </div><!-- content //-->
</div><!-- footer //-->

<div class="copyright">
    <div class="content">
        <?php
            $theme_opts                     =       get_option('mv_opts');
            echo stripslashes_deep($theme_opts['footer']);
        ?>
    </div>
</div><!-- copyright //-->


</div><!-- wrap //-->
</div><!-- container //-->

<?php wp_footer();?>

</body>
</html>