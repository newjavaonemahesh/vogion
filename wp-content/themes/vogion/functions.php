<?php

// Set up
add_theme_support('menus');
add_theme_support('post-thumbnails');



//  Includes
include(get_template_directory() . '/includes/front/enqueue.php');
include(get_template_directory() . '/includes/setup.php');
include(get_template_directory() . '/includes/widgets.php');
include(get_template_directory() . '/includes/activate.php');
include(get_template_directory() . '/includes/admin/menus.php');
include(get_template_directory() . '/includes/admin/options-page.php');
include(get_template_directory() . '/includes/admin/init.php');
include(get_template_directory() . '/process/save-options.php');


//  Action and Filter Hooks
add_action('wp_enqueue_scripts', 'mv_enqueue');
add_action('after_setup_theme', 'mv_setup_theme');
add_action('widgets_init', 'mv_widgets');
add_action('after_switch_theme', 'mv_activate');
add_action('admin_menu', 'mv_admin_menus');
add_action('admin_init', 'mv_admin_init');


//  Shortcodes