<?php

function mv_save_options(){
    if (!current_user_can('edit_theme_options')) {
        wp_die(__('You are not allowed to be on this page.'));
    }

    check_admin_referer('mv_options_verify');

    $opts               =     get_option('mv_opts');

    $opts['footer']                =     $_POST['mv_inputFooter'];
    $opts['logo_img']                =     esc_url_raw($_POST['mv_inputLogoImg']);

    update_option('mv_opts', $opts);
    wp_redirect(admin_url('admin.php?page=mv_theme_opts&status=1'));

}