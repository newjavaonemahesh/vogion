<?php get_header();?>

<div id="main">

    <div id="content">

        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>

                <div class="post">


                    <h1 class="title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>

                    <div class="text">
                        <?php the_content();?>
                        <div class="clear"></div>
                    </div>
                    <!-- .text //-->

                </div><!-- .post //-->
            <?php
            }
        }
        ?>

    </div><!-- content //-->

    <ul id="sidebar" class="noul">
        <?php get_sidebar();?>
    </ul><!-- sidebar //-->
    <div class="clear"> </div>

</div><!-- main //-->
<?php get_footer();?>
